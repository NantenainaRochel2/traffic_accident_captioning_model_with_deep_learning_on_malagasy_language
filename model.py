#!/usr/bin/python3
# -*- coding: utf-8 -*-

from keras.layers import Input, Dropout, Embedding, Dense, LSTM
from keras.layers.merge import add
from keras.models import Model
from tensorflow.keras.utils import plot_model
import tensorflow as tf

# define the captioning model
def define_model(vocab_size, max_length):
    # feature extractor model
    inputs1 = Input(shape=(4096,))
    fe1 = Dropout(0.5)(inputs1)
    fe2 = Dense(256, activation='relu')(fe1)
    # sequence model
    inputs2 = Input(shape=(max_length,))
    se1 = Embedding(vocab_size, 256, mask_zero=True)(inputs2)
    se2 = Dropout(0.5)(se1)
    se3 = LSTM(256)(se2)
    # decoder model
    decoder1 = add([fe2, se3])
    decoder2 = Dense(256, activation='relu')(decoder1)
    decoder3 = Dropout(0.2)(decoder2)
    outputs = Dense(vocab_size, activation='softmax')(decoder3)
    # tie it together [image, seq] [word]
    model = Model(inputs=[inputs1, inputs2], outputs=outputs)
    #opt = tf.keras.optimizers.Adam(learning_rate=0.01)
    #opt = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.0, name="SGD")
    #opt = tf.keras.optimizers.SGD(learning_rate=0.008, momentum=0.0, name="SGD")
    opt = tf.keras.optimizers.SGD(learning_rate=0.006, momentum=0.0, name="SGD")
    model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
    # summarize model
    print(model.summary())
    plot_model(model, to_file='generatedData/model/model.png', show_shapes=True)
    return model