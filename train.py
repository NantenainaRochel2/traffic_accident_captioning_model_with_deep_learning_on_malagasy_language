#!/usr/bin/python3
# -*- coding: utf-8 -*-

from load import load_set, load_clean_descriptions, load_photo_features
from preprocess import create_tokenizer, max_length, create_sequences, create_sequences_with_progressice_loading
from keras.callbacks import ModelCheckpoint
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from model import define_model
from pickle import dump
import matplotlib.pyplot as plt

# data generator, intended to be used in a call to model.fit_generator()
def data_generator_with_progressive_loading(descriptions, photos, tokenizer, max_len, vocab_size):
	# loop for ever over images
	while 1:
		for key, desc_list in descriptions.items():
			# retrieve the photo feature
			photo = photos[key][0]
			in_img, in_seq, out_word = create_sequences_with_progressice_loading(tokenizer, max_len, desc_list, photo, vocab_size)
			yield ([in_img, in_seq], out_word)

def train():
    ###############################################
    ############## train dataset ##################
    ###############################################

    #load train dataset
    filename = "gasCAR_03k/gasCAR_03k_txt/gasCAR_03k.trainImg.txt"
    train = load_set(filename)
    print("Dataset : %d" % len(train))

    #descriptions
    train_descriptions = load_clean_descriptions("generatedData/descriptions.txt", train)
    print("Description : train = %d" % len(train_descriptions))

    # photo features
    train_features = load_photo_features("generatedData/Pickle/features.pkl", train)
    print("Photo : train = %d" % len(train_features))

    #prepare tokenizer
    tokenizer = create_tokenizer(train_descriptions)
    vocab_size = len(tokenizer.word_index) + 1
    print("Vocabulary Size : %d" % vocab_size)

    #save tokenizer
    dump(tokenizer, open("generatedData/Pickle/tokenizer.pkl", "wb"))

    #determine the maximuim sequences length
    max_len = max_length(train_descriptions)
    print("description length : %d" % max_len)

    #prepare sequences
    X1train, X2train, ytrain, = create_sequences(tokenizer, max_len, train_descriptions, train_features, vocab_size)

    ###############################################
    ############## test dataset ##################
    ###############################################

    #load test dataset
    filename = "gasCAR_03k/gasCAR_03k_txt/gasCAR_03k.testImg.txt"
    test = load_set(filename)
    print("Dataset : %d" % len(test))

    #descriptions
    test_descriptions = load_clean_descriptions("generatedData/descriptions.txt", test)
    print("Description : test = %d" % len(test_descriptions))

    # photo features
    test_features = load_photo_features("generatedData/Pickle/features.pkl", test)
    print("Photo : test = %d" % len(test_features))

    #prepare sequences
    X1test, X2test, ytest, = create_sequences(tokenizer, max_len, test_descriptions, test_features, vocab_size)

    ###############################################
    ############## train model ######################
    ###############################################

    #define model
    model = define_model(vocab_size, max_len)

    #define the checkpoint callback
    filepath = 'model/model-ep{epoch:03d}-loss{loss:.3f}-val_loss{val_loss:.3f}.h5'
    checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=True, mode='min')

    #Features Scaling
    sc = StandardScaler()
    X1train = sc.fit_transform(X1train)
    X2train = sc.fit_transform(X2train)
    X1test = sc.transform(X1test)
    X2test = sc.transform(X2test)

    #Appluy PCA
    pca = PCA(n_components=2)
    X1train = pca.fit_transform(X1train)
    X2train = pca.fit_transform(X2train)
    X1test = pca.transform(X1test)
    X2test = pca.transform(X2test)

    #fit the model
    model_trained = model.fit([X1train, X2train], ytrain, epochs=801, verbose=1, callbacks=[checkpoint], validation_data=([X1test, X2test], ytest))
    accuracy = model_trained.history['accuracy']
    accuracy_eval = model_trained.history['val_accuracy']
    loss = model_trained.history['loss']
    loss_eval = model_trained.history['val_loss']

    epochs = range(len(accuracy))
    plt.figure(figsize=[5, 5])
    plt.subplot(121)
    plt.plot(epochs, accuracy, 'bo', label="Training accuracy")
    plt.plot(epochs, accuracy_eval, 'b', label="Validation Accuracy")
    plt.title("Training and validation accuracy")
    plt.legend()
    plt.subplot(122)
    plt.plot(loss, 'bo', label="Training loss")
    plt.plot(epochs, loss_eval, 'b', label="Validation loss")
    plt.title("Training and validation loss")
    plt.legend()
    plt.savefig('generatedData/model/image_captioning_accuracy.png')
    plt.show()

def train_with_progresssive_loading():
    ###############################################
    ############## train dataset ##################
    ###############################################

    #load train dataset
    filename = "gasCAR_03k/gasCAR_03k_txt/gasCAR_03k.trainImg.txt"
    train = load_set(filename)
    print("Dataset : %d" % len(train))

    #descriptions
    train_descriptions = load_clean_descriptions("generatedData/descriptions.txt", train)
    print("Description : train = %d" % len(train_descriptions))

    # photo features
    train_features = load_photo_features("generatedData/Pickle/features.pkl", train)
    print("Photo : train = %d" % len(train_features))

    #prepare tokenizer
    tokenizer = create_tokenizer(train_descriptions)
    vocab_size = len(tokenizer.word_index) + 1
    print("Vocabulary Size : %d" % vocab_size)

    #save tokenizer
    dump(tokenizer, open("generatedData/Pickle/tokenizer.pkl", "wb"))

    #determine the maximuim sequences length
    max_len = max_length(train_descriptions)
    print("description length : %d" % max_len)

    ###############################################
    ############## train model ######################
    ###############################################

    #define model
    model = define_model(vocab_size, max_len)

    # train the model, run epochs manually and save after each epoch
    epochs = 3001 #5001
    steps = len(train_descriptions)
    for i in range(epochs):
        # create the data generator
        generator = data_generator_with_progressive_loading(train_descriptions, train_features, tokenizer, max_len, vocab_size)
        # fit for one epoch
        model.fit_generator(generator, epochs=1, steps_per_epoch=steps, verbose=1)
        # save model
        model.save('model/model_' + str(i) + '.h5')

if __name__=="__main__":
    train()
    #train_with_progresssive_loading()