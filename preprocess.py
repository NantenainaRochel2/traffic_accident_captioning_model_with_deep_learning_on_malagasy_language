import string
from numpy import array
from load import load_doc, load_descriptions
from keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical
from keras.preprocessing.sequence import pad_sequences

def clean_descriptions(descriptions):
    #prepare translation table for removing punctiation
    table = str.maketrans('', '', string.punctuation)
    for key, desc_list in descriptions.items():
        for i in range(len(desc_list)):
            desc = desc_list[i]
            #tokenize
            desc = desc.split()
            #convert to lower case
            desc = [word.lower() for word in desc]
            #remove punctiation from each token
            desc = [w.translate(table) for w in desc]
            #removing tokens with numbers in them
            #desc = [word for word in desc if word.isalpha()]
            #store as string
            desc_list[i] = ' '.join(desc)

#convert the loaded descriptions into a vocabulary of words
def to_vocabulary(descriptions):
    #build a list of all descriptions strings
    all_desc = set()
    for key in descriptions.keys():
        [all_desc.update(d.split()) for d in descriptions[key]]
    return all_desc

#save descriptions to file, oe per line
def save_descriptions(descriptions, filename):
    lines = list()
    for key, desc_list in descriptions.items():
        for desc in desc_list:
            lines.append(key + ' ' + desc)
    data = '\n'.join(lines)
    file = open(filename, 'w') #, encoding='iso-8859-1'
    file.write(data)
    file.close()

#convert a dictionary of clean descriptions to a list descriptions
def to_lines(descriptions):
    all_desc = list()
    for key in descriptions.keys():
        [all_desc.append(d) for d in descriptions[key]]
    return all_desc

#fit a tokenizer given caption descriptions
def create_tokenizer(descriptions):
    lines = to_lines(descriptions)
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(lines)
    return tokenizer

# create sequences of images, input sequences and output words for an image
def create_sequences(tokenizer, max_length, descriptions, photos, vocab_size):
	X1, X2, y = list(), list(), list()
	# walk through each image identifier
	for key, desc_list in descriptions.items():
		# walk through each description for the image
		for desc in desc_list:
			# encode the sequence
			seq = tokenizer.texts_to_sequences([desc])[0]
			# split one sequence into multiple X,y pairs
			for i in range(1, len(seq)):
				# split into input and output pair
				in_seq, out_seq = seq[:i], seq[i]
				# pad input sequence
				in_seq = pad_sequences([in_seq], maxlen=max_length)[0]
				# encode output sequence
				out_seq = to_categorical([out_seq], num_classes=vocab_size)[0]
				# store
				X1.append(photos[key][0])
				X2.append(in_seq)
				y.append(out_seq)
	return array(X1), array(X2), array(y)

# create sequences of images, input sequences and output words for an image
def create_sequences_with_progressice_loading(tokenizer, max_len, desc_list, photo, vocab_size):
    X1, X2, y = list(), list(), list()
    # walk through each description for the image
    for desc in desc_list:
        # encode the sequence
        seq = tokenizer.texts_to_sequences([desc])[0]
        # split one sequence into multiple X,y pairs
        for i in range(1, len(seq)):
            # split into input and output pair
            in_seq, out_seq = seq[:i], seq[i]
            # pad input sequence
            in_seq = pad_sequences([in_seq], maxlen=max_len)[0]
            # encode output sequence
            out_seq = to_categorical([out_seq], num_classes=vocab_size)[0]
            # store
            X1.append(photo)
            X2.append(in_seq)
            y.append(out_seq)
    return array(X1), array(X2), array(y)

#calculate the length of the descriptions with the mosts words
def max_length(descriptions):
    lines = to_lines(descriptions)
    return max(len(d.split()) for d in lines)

#map an integer to a word
def word_for_id(integer, tokenizer):
    for word, index in tokenizer.word_index.items():
        if index == integer:
            return word
    return None

if __name__=="__main__":
    filename = "gasCAR_03k/gasCAR_03k_txt/gasCAR_03k.token.txt"
    #load descriptions
    doc = load_doc(filename)
    #print("doc : ", doc)
    descriptions = load_descriptions(doc)
    #print("description : %s" % descriptions)
    print("loaded : %d" % len(descriptions))
    #clean descriptions
    clean_descriptions(descriptions)
    #summarize vocabulary
    vocabulary = to_vocabulary(descriptions)
    with open("generatedData/vocabs.txt", "w") as fichia:
        for vocab in vocabulary:
            fichia.write(vocab+"\n")
    print(vocabulary)
    print('Vocabulary Size : %d' % len(vocabulary))
    #save descriptions
    save_descriptions(descriptions, "generatedData/descriptions.txt")