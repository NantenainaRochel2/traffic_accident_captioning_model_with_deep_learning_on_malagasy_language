#!/usr/bin/python3
# -*- coding: utf-8 -*-

from os import listdir
from pickle import dump, load
from keras.applications.vgg16 import VGG16, preprocess_input
from keras.preprocessing.image import load_img, img_to_array
from keras.models import Model

#extract features from each photo in the directory
def extract_features(directory):
    #load model
    model = VGG16()
    #re structured the model
    model = Model(inputs=model.inputs, outputs=model.layers[-2].output)
    #summarize
    print(model.summary())
    #extract features from each photo
    features = dict()
    for name in listdir(directory):
        print(name)
        #load the image from file
        filename = directory + "/" + name
        image = load_img(filename, target_size=(224, 224))
        #convert the image pixels to numpy array
        image = img_to_array(image)
        #reshape data for models
        image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
        #prepare the image for the VGG model
        image = preprocess_input(image)
        #get features
        feature = model.predict(image, verbose=0)
        #get image id
        image_id = name.split('.')[0]
        #store features
        features[image_id] = feature
        print('name : %s' % name)
    return features

#extract features from each photo in the directory
def extract_features_caption(filename):
    #load model
    model = VGG16()
    #re structured the model
    model = Model(inputs=model.inputs, outputs=model.layers[-2].output)
    #load photo
    image = load_img(filename, target_size = (224, 224))
    #convert the image pixels to numpy array
    image = img_to_array(image)
    #reshape data for models
    image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
    #prepare the image for the VGG model
    image = preprocess_input(image)
    #get features
    feature = model.predict(image, verbose=0)
    return feature

#load photo features
def load_photo_features(filename, dataset):
    #load all photo features
    all_features = load(open(filename, "rb"))
    #filter features
    features = {k: all_features[k] for k in dataset}
    return features

#load doc into memory
def load_doc(filename):
    #open the file as readonly
    file = open(filename, "r", encoding='iso-8859-1') #encoding="utf-8" #encoding="ISO-8859-1"
    #read all the text
    text = file.read()
    #close the file
    file.close()
    return text

#load pre-defined list of photo identifiers
def load_set(filename):
    doc = load_doc(filename)
    dataset = list()
    #process line by line
    for line in doc.split('\n'):
        #skip empty lines
        if(len(line) < 1):
            continue
        #get the image identifier
        identifier = line.split('.')[0]
        dataset.append(identifier)
    return set(dataset)

#extract descriptions from image
def load_descriptions(doc):
    mapping = dict()
    #process lines
    for line in doc.split('\n'):
        #split line by white space
        tokens = line.split()
        if(len(line) < 2):
            continue
        #take the first tokens as the image id, the rest as the description
        image_id, image_desc = tokens[0], tokens[1:]
        #remove filename from image id
        image_id = image_id.split('.')[0]
        #convert description tokens back to string
        image_desc = ' '.join(image_desc)
        #create the list if needed
        if image_id not in mapping:
            mapping[image_id] = list()
        #store descriptions
        mapping[image_id].append(image_desc)
    return mapping

#load clean descriptions into memory
def load_clean_descriptions(filename, dataset):
    #load document
    doc = load_doc(filename)
    descriptions = dict()
    for line in doc.split('\n'):
        #split line by white space
        tokens = line.split()
        #split id from description
        image_id, image_desc = tokens[0], tokens[1:]
        #skip image not in the set
        if image_id in dataset:
            #create list
            if image_id not in descriptions:
                descriptions[image_id] = list()
            #wrap descriptions in tokens
            desc = 'startseq ' + ' '.join(image_desc) + ' endseq'
            #store
            descriptions[image_id].append(desc)
    return descriptions

if __name__=="__main__":
    directory = "gasCAR_03k/gasCAR_03k_img/"
    features = extract_features(directory)
    print("Extractred : %s" % len(features))
    #save to file
    dump(features, open("generatedData/Pickle/features.pkl", "wb"))

    filename = "gasCAR_03k/gasCAR_03k_txt/gasCAR_03k.token.txt"
    #load descriptions
    doc = load_doc(filename)
    print("doc : \n %s" % doc)
    descriptions = load_descriptions(doc)
    print("description : %s" % descriptions)
    print("loaded : %d" % len(descriptions))

    filename = "gasCAR_03k/gasCAR_03k_txt/gasCAR_03k.trainImg.txt"
    train = load_set(filename)
    print("Dataset : %d" % len(train))
    #description
    train_descriptions = load_clean_descriptions("generatedData/descriptions.txt", train)
    print("Description : train = %s\n" % train_descriptions)
    print("Description : train = %d" % len(train_descriptions))
    #photo features
    train_features = load_photo_features("generatedData/Pickle/features.pkl", train)
    #print("Photo : train = %s\n" % train_features)
    print("Photo : train = %d" % len(train_features))