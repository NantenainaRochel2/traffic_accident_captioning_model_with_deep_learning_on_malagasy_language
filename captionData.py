#!/usr/bin/python3
# -*- coding: utf-8 -*-

from pickle import load
from numpy import argmax
from load import extract_features_caption
from preprocess import word_for_id
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences

# generate a description for an image
def generate_desc(model, tokenizer, photo, max_length):
	# seed the generation process
	in_text = 'startseq'
	# iterate over the whole length of the sequence
	for i in range(max_length):
		# integer encode input sequence
		sequence = tokenizer.texts_to_sequences([in_text])[0]
		# pad input
		sequence = pad_sequences([sequence], maxlen=max_length)
		# predict next word
		yhat = model.predict([photo,sequence], verbose=0)
		# convert probability to integer
		yhat = argmax(yhat)
		# map integer to word
		word = word_for_id(yhat, tokenizer)
		# stop if we cannot map the word
		if word is None:
			break
		# append as input for generating the next word
		in_text += ' ' + word
		# stop if we predict the end of the sequence
		if word == 'endseq':
			break
	return in_text

def generate_caption():
	# load the tokenizer
	tokenizer = load(open('generatedData/Pickle/tokenizer.pkl', 'rb'))
	# pre-define the max sequence length (from training)
	max_length = 23

	# load the model
	# model = load_model('model/old_model_01/model_2276.h5')
	model = load_model('model/model-ep801-loss0.700-val_loss0.446.h5')
	# load and prepare the photograph
	photo = extract_features_caption('data/image_to_be_captioned/tuctuc.jpg')
	#generate description
	description = generate_desc(model, tokenizer, photo, max_length)
	print(">seed : \n", description)

def generateCaption(image):
	# load the tokenizer
	tokenizer = load(open('generatedData/Pickle/tokenizer.pkl', 'rb'))
	# pre-define the max sequence length (from training)
	max_length = 23
	# load the model
	model = load_model('model/model-ep801-loss0.700-val_loss0.446.h5') #model-ep004-loss2.498-val_loss3.861.h5')
	# load and prepare the photograph
	photo = extract_features_caption(image)
	#generate description
	description = generate_desc(model, tokenizer, photo, max_length)
	print(">seed : \n", description)
	description = description.replace('startseq', '')
	description = description.replace('endseq', '')
	return description

if __name__=="__main__":
	generate_caption()